$( document ).ready(function() {
    var base_color = "rgb(230,230,230)";
    var active_color = "rgb(211, 166, 37)";

    function shuffleArray(oldArray) {
        var array = oldArray
        for (var i = array.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
        return array;
    }
    function polarToCartesian(centerX, centerY, radius, angleInDegrees) {
        var angleInRadians = (angleInDegrees-90) * Math.PI / 180.0;

        return {
            x: centerX + (radius * Math.cos(angleInRadians)),
            y: centerY + (radius * Math.sin(angleInRadians))
        };
    }

    function describeArc(x, y, radius, startAngle, endAngle){

        var start = polarToCartesian(x, y, radius, endAngle);
        var end = polarToCartesian(x, y, radius, startAngle);

        var arcSweep = endAngle - startAngle <= 180 ? "0" : "1";

        var d = [
            "M", start.x, start.y,
            "A", radius, radius, 0, arcSweep, 0, end.x, end.y,
            "L", x,y,
            "L", start.x, start.y
        ].join(" ");

        return d;
    }


    function refreshAnswers() {
        var answers = {
            h:0, g:0, s:0, r:0
        };
        for (let i = 0; i < questions.length; i++) {
            const input = $('input[name="q'+i+'"]:checked');
            if(input.length>0) {
                const values = input.val().split('|');
                for (let j = 0; j < values.length; j++) {
                    answers[values[j]]++;
                }
            }
        }

        var answers_sorted = [
            {name:'Gryffondor', value: answers.g, id: 'g'},
            {name:'Serpentard', value: answers.s, id: 's'},
            {name:'Poufsouffle', value: answers.h, id: 'h'},
            {name:'Serdaigle', value: answers.r, id: 'r'},
        ]
        answers_sorted.sort(function (a, b) {
            return b.value - a.value;
        });

        const final = $('section.answers');
        const arms = final.find('.arms');
        final.empty();

        final.append(arms);

        var total = 0;
        for (let i = 0; i < answers_sorted.length; i++) {
            answers_sorted[i].value = Math.round(answers_sorted[i].value/questions.length*100);
            if(i>0) {
                answers_sorted[i].selected = (Math.abs(answers_sorted[i].value-answers_sorted[0].value)<5);
            } else {
                answers_sorted[i].selected = true;
            }
            const element = $('<p class="answer"><span class="tick"></span>'+answers_sorted[i].name+' : '+answers_sorted[i].value+'%<span class="progress"></span></p>');
            if(answers_sorted[i].selected===true) element.addClass('selected');
            final.append(element);
            const startAngle = total;
            var endAngle = total = answers_sorted[i].value + total;
            if(i === answers_sorted.length-1) endAngle = total = 100;
            arms.find('.arm-'+answers_sorted[i].id).css('clip-path', 'path("'+describeArc(75,75,150,startAngle*3.6, endAngle*3.6)+'")');
        }

        return final;
    }

    var whereToInsert = $('.questions');
    for (let i = 0; i < questions.length; i++) {
        const question = questions[i];
        const newElement = $('<section>');
        newElement.append($('<p>'+question.title+'</p>'));

        const answers = shuffleArray(question.answers);
        for (let j = 0; j < answers.length; j++) {
            const id = 'q'+i+'-'+j;
            newElement.append($('<label for="'+id+'">\n' +
                '        <input type="radio" name="q'+i+'" id="'+id+'" value="'+answers[j].value+'">' + answers[j].title +
                '    </label>'))
        }
        newElement.insertAfter(whereToInsert);
        whereToInsert = newElement;
    }
    $('.questions').remove();

    var child = 1;
    var length = $("section").length - 1;
    $("#prev").addClass("disabled");
    $("#submit").addClass("disabled");

    $("section").not("section:nth-of-type(1)").hide();
    $("section").not("section:nth-of-type(1)").css('transform','translateX(100px)');

    var svgWidth = length * 200 + 24;
    $("#svg_wrap").html(
        '<svg version="1.1" id="svg_form_time" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 ' +
        svgWidth +
        ' 24" xml:space="preserve"></svg>'
    );

    function makeSVG(tag, attrs) {
        var el = document.createElementNS("http://www.w3.org/2000/svg", tag);
        for (var k in attrs) el.setAttribute(k, attrs[k]);
        return el;
    }

    for (i = 0; i < length; i++) {
        var positionX = 12 + i * 200;
        var rect = makeSVG("rect", { x: positionX, y: 9, width: 200, height: 6 });
        document.getElementById("svg_form_time").appendChild(rect);
        // <g><rect x="12" y="9" width="200" height="6"></rect></g>'
        var circle = makeSVG("circle", {
            cx: positionX,
            cy: 12,
            r: 12,
            width: positionX,
            height: 6
        });
        document.getElementById("svg_form_time").appendChild(circle);
    }

    var circle = makeSVG("circle", {
        cx: positionX + 200,
        cy: 12,
        r: 12,
        width: positionX,
        height: 6
    });
    document.getElementById("svg_form_time").appendChild(circle);

    $('#svg_form_time rect').css('fill',base_color);
    $('#svg_form_time circle').css('fill',base_color);
    $("circle:nth-of-type(1)").css("fill", active_color);


    $(".button").click(function () {
        var id = $(this).attr("id");
        if (id == "next") {
            $("#prev").removeClass("disabled");
            if (child >= length) {
                $(this).addClass("disabled");
                $('#submit').removeClass("disabled");
            }
            if (child <= length) {
                child++;
            }
            refreshAnswers();
        } else if (id == "prev") {
            $("#next").removeClass("disabled");
            $('#submit').addClass("disabled");
            if (child <= 2) {
                $(this).addClass("disabled");
            }
            if (child > 1) {
                child--;
            }
        }
        $("#svg_form_time rect").css("fill", active_color);
        $("#svg_form_time circle").css("fill", active_color);
        var circle_child = child + 1;
        $("#svg_form_time rect:nth-of-type(n + " + child + ")").css(
            "fill",
            base_color
        );
        $("#svg_form_time circle:nth-of-type(n + " + circle_child + ")").css(
            "fill",
            base_color
        );
        var currentSection = $("section:nth-of-type(" + child + ")");
        currentSection.fadeIn();
        currentSection.css('transform','translateX(0)');
        currentSection.prevAll('section').css('transform','translateX(-100px)');
        currentSection.nextAll('section').css('transform','translateX(100px)');
        $('section').not(currentSection).hide();
    });

});